﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ding_techical_test
{
    public class RequestMessage : Message
    {
        private string companyIdentifier;
        private int messageID;

        public string CompanyIdentifier
        {
            get
            {
                return companyIdentifier;
            }

            set
            {
                companyIdentifier = value;
            }
        }

        public int MessageID
        {
            get
            {
                return messageID;
            }

            set
            {
                messageID = value;
            }
        }

        public RequestMessage() : base()
        {
            companyIdentifier = "noCompany";
            messageID = -1;
        }

        public RequestMessage(string compIdentifier, string msgDate, string msgTime, int msgID, string phoneNum, string amount) 
            : base(msgDate, msgTime, phoneNum, amount)
        {
            companyIdentifier = compIdentifier;
            messageID = msgID;
        }

        public RequestMessage(string compIdentifier, int msgID, string phoneNum, string amount)
            : base(phoneNum, amount)
        {
            companyIdentifier = compIdentifier;
            messageID = msgID;
        }

        public override void print()
        {
            base.print();
            Console.WriteLine("Company Identifier: {0} \nMessage ID: {1}", companyIdentifier, messageID);
        }

        //this function will create XmlDocument with header and body and populate it with attributes from this class
        public XmlDocument toXMLFormat()
        {

            XmlDocument doc = new XmlDocument();
            XmlElement el = (XmlElement)doc.AppendChild(doc.CreateElement("Message"));
            XmlNode headerNode = el.AppendChild(doc.CreateElement("Header"));
            headerNode.AppendChild(doc.CreateElement("Identifier")).InnerText = companyIdentifier;
            headerNode.AppendChild(doc.CreateElement("MessageDate")).InnerText = base.MsgDate;
            headerNode.AppendChild(doc.CreateElement("MessageTime")).InnerText = base.MsgTime;
            
            XmlNode bodyNode = el.AppendChild(doc.CreateElement("Body"));
            bodyNode.AppendChild(doc.CreateElement("MessageID")).InnerText = messageID.ToString();
            bodyNode.AppendChild(doc.CreateElement("PhoneNumber")).InnerText = base.PhoneNumber;
            bodyNode.AppendChild(doc.CreateElement("Amount")).InnerText = base.Amount;

            return doc;
        }

        



    }
}
