﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ding_techical_test
{
    public class ResponseMessage : Message
    {
        private int transactionID;
        private int transactionNumber;
        private string result;

        public int TransactionID
        {
            get
            {
                return transactionID;
            }

            set
            {
                transactionID = value;
            }
        }

        public int TransactionNumber
        {
            get
            {
                return transactionNumber;
            }

            set
            {
                transactionNumber = value;
            }
        }

        public string Result
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
            }
        }

        public ResponseMessage() : base()
        {
            transactionID = 0;
            transactionNumber = 0;
            result = "noResult";
        }

        public ResponseMessage(string messageDate, string messageTime, int tranID, int tranNum, string phoneNum, string amount, string res) 
            : base(messageDate, messageTime, phoneNum, amount)
        {
            transactionID = tranID;
            transactionNumber = tranNum;
            result = res;
        }

        public void processResponseCode()
        {
            //depending on the code choose what to do
            if(result == "01")
            {
                
                Console.WriteLine("Processing response Succes");
            }else if(result == "02")
            {
                //if needed the code can be replaced with 99
                result = "99";
                Console.WriteLine("Processing response Failure");
            }
            else if(result == "03")//i am assuming that 03 means invalid message
            {
                //i can change the 03 code to 999 here if needet
                result = "999";
                Console.WriteLine("Processing Invalid Message");
            }
            else
            {
                Console.WriteLine("Unknow result code");
                //maybe throw exception here or deal with it same as invalid message
            }

        }

        public override void print()
        {
            base.print();
            Console.WriteLine("TransactionID: {0} \nTransaction Number: {1} \nResul: {2}", transactionID, transactionNumber,result);
        }

        //if I would like the date and time be formatted diferently according some specification I could do it here
        public void printFormatted()
        {
            Console.WriteLine("Response message:");
            Console.WriteLine("Message Date: {0} \nMessage Time: {1} \nTransaction ID: {2} \nTransaction Number: {3} \nPhone Number: {4} \nAmount: {5} \nResult {6}",
                base.MsgDate,base.MsgTime,transactionID,transactionNumber,base.PhoneNumber,base.Amount,result);
        }
    }


}
