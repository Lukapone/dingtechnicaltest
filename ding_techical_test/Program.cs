﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace ding_techical_test
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Lukas Ponik Ding Technical Test.\n");
            
            MessageHandler msgHandler = new MessageHandler();
            RequestMessage msg;
            ResponseMessage respMessage;
            byte[] messageInBytes;
            byte[] buffer;

            msg = createMsg1();
            if (msg.isValidMessage())
            {
                messageInBytes = msgHandler.buildXMLRequest(msg);
                dummySendAndPrintInHex(messageInBytes);
            }
            else
            {
                Console.WriteLine("Invalid message please check the error messge.");
            }

            buffer = new byte[] { 69, 90, 69, 45, 88, 77, 76, 45, 77, 115, 103, 48, 50, 60, 77, 101, 115,
                115, 97, 103, 101, 62, 13, 10, 32, 32, 60, 72, 101, 97, 100, 101, 114, 62, 13, 10, 32, 32, 32,
                32, 60, 77, 101, 115, 115, 97, 103, 101, 68, 97, 116, 101, 62, 50, 48, 49, 48, 48, 51, 50, 52, 60,
                47, 77, 101, 115, 115, 97, 103, 101, 68, 97, 116, 101, 62, 13, 10, 32, 32, 32, 32, 60, 77, 101, 115,
                115, 97, 103, 101, 84, 105, 109, 101, 62, 49, 57, 50, 54, 48, 50, 60, 47, 77, 101, 115, 115, 97, 103,
                101, 84, 105, 109, 101, 62, 13, 10, 32, 32, 60, 47, 72, 101, 97, 100, 101, 114, 62, 13, 10, 32, 32, 60,
                66, 111, 100, 121, 62, 13, 10, 32, 32, 32, 32, 60, 84, 114, 97, 110, 115, 97, 99, 116, 105, 111, 110, 73,
                68, 62, 51, 51, 50, 53, 50, 54, 60, 47, 84, 114, 97, 110, 115, 97, 99, 116, 105, 111, 110, 73, 68, 62, 13,
                10, 32, 32, 32, 32, 60, 84, 114, 97, 110, 115, 97, 99, 116, 105, 111, 110, 78, 117, 109, 98, 101, 114, 62,
                49, 50, 49, 48, 52, 53, 50, 60, 47, 84, 114, 97, 110, 115, 97, 99, 116, 105, 111, 110, 78, 117, 109, 98,
                101, 114, 62, 13, 10, 32, 32, 32, 32, 60, 80, 104, 111, 110, 101, 78, 117, 109, 98, 101, 114, 62, 54, 51,
                48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 60, 47, 80, 104, 111, 110, 101, 78, 117, 109, 98, 101, 114, 62, 13,
                10, 32, 32, 32, 32, 60, 65, 109, 111, 117, 110, 116, 62, 48, 48, 48, 48, 48, 48, 50, 53, 48, 48, 60, 47, 65,
                109, 111, 117, 110, 116, 62, 13, 10, 32, 32, 32, 32, 60, 82, 101, 115, 117, 108, 116, 62, 48, 49, 60, 47, 82,
                101, 115, 117, 108, 116, 62, 13, 10, 32, 32, 60, 47, 66, 111, 100, 121, 62, 13, 10, 60, 47, 77, 101, 115, 115, 97, 103, 101, 62 };


            Console.WriteLine("Simulating receive of the first response through the socket.\n");
            respMessage = msgHandler.parseByteArray(buffer);
            if(respMessage.isValidMessage())
            {
                respMessage.printFormatted();
                //you can have function to check the result codes and further deal with the messages
                respMessage.processResponseCode();
            }
            else
            {
                Console.WriteLine("Invalid response message please check the error messge.");
            }
           


            //simulate sending of the second message
            msg = createMsg2();
            if(msg.isValidMessage())
            {
                messageInBytes = msgHandler.buildXMLRequest(msg);
                dummySendAndPrintInHex(messageInBytes);
            }
            else
            {
                Console.WriteLine("Invalid message please check the error messge.");
            }

            buffer = new byte[] { 69, 90, 69, 45, 88, 77, 76, 45, 77, 115, 103, 48, 50, 60, 77,
                101, 115, 115, 97, 103, 101, 62, 13, 10, 32, 32, 60, 72, 101, 97, 100, 101, 114, 62,
                13, 10, 32, 32, 32, 32, 60, 77, 101, 115, 115, 97, 103, 101, 68, 97, 116, 101, 62, 50,
                48, 49, 48, 48, 51, 50, 52, 60, 47, 77, 101, 115, 115, 97, 103, 101, 68, 97, 116, 101,
                62, 13, 10, 32, 32, 32, 32, 60, 77, 101, 115, 115, 97, 103, 101, 84, 105, 109, 101, 62,
                49, 57, 50, 56, 48, 54, 60, 47, 77, 101, 115, 115, 97, 103, 101, 84, 105, 109, 101, 62,
                13, 10, 32, 32, 60, 47, 72, 101, 97, 100, 101, 114, 62, 13, 10, 32, 32, 60, 66, 111, 100,
                121, 62, 13, 10, 32, 32, 32, 32, 60, 84, 114, 97, 110, 115, 97, 99, 116, 105, 111, 110, 73,
                68, 62, 51, 51, 50, 53, 50, 55, 60, 47, 84, 114, 97, 110, 115, 97, 99, 116, 105, 111, 110, 73,
                68, 62, 13, 10, 32, 32, 32, 32, 60, 84, 114, 97, 110, 115, 97, 99, 116, 105, 111, 110, 78, 117,
                109, 98, 101, 114, 62, 49, 50, 49, 48, 52, 55, 48, 60, 47, 84, 114, 97, 110, 115, 97, 99, 116,
                105, 111, 110, 78, 117, 109, 98, 101, 114, 62, 13, 10, 32, 32, 32, 32, 60, 80, 104, 111, 110,
                101, 78, 117, 109, 98, 101, 114, 62, 54, 51, 57, 57, 57, 57, 57, 57, 57, 57, 57, 57, 60, 47,
                80, 104, 111, 110, 101, 78, 117, 109, 98, 101, 114, 62, 13, 10, 32, 32, 32, 32, 60, 65, 109,
                111, 117, 110, 116, 62, 48, 48, 48, 48, 48, 48, 50, 53, 48, 48, 60, 47, 65, 109, 111, 117,
                110, 116, 62, 13, 10, 32, 32, 32, 32, 60, 82, 101, 115, 117, 108, 116, 62, 48, 51, 60, 47,
                82, 101, 115, 117, 108, 116, 62, 13, 10, 32, 32, 60, 47, 66, 111, 100, 121, 62, 13, 10,
                60, 47, 77, 101, 115, 115, 97, 103, 101, 62 };

            Console.WriteLine("Simulating receive of the second response through the socket.\n");
            respMessage = msgHandler.parseByteArray(buffer);
            if (respMessage.isValidMessage())
            {
                respMessage.printFormatted();
                respMessage.processResponseCode();
            }
            else
            {
                Console.WriteLine("Invalid response message please check the error messge.");
            }


            Console.Read();
        }
     
        static RequestMessage createMsg1()
        {
            string companyId = "EZE";
            int msgId = 332526;
            string phoneNum = "630000000000";
            string amount = "25";
            //date and time stamp are upon creation if you would like to use it later in the process you can call the function: msg.dateTimeStampMessage()
            return new RequestMessage(companyId, msgId, phoneNum, amount);
        }

        static RequestMessage createMsg2()
        {
            string companyId = "EZE";
            int msgId = 332527;
            string phoneNum = "639999999999";
            string amount = "25";
            
            return new RequestMessage(companyId, msgId, phoneNum, amount);
        }

        public static string xmlToString(XmlDocument doc)//http://stackoverflow.com/questions/2407302/convert-xmldocument-to-string
        {
            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                doc.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                return stringWriter.GetStringBuilder().ToString();
            }
        }

        static void dummySendAndPrintInHex(byte[] msgToSend)
        {
            Console.WriteLine("\nSending message through the socket.\n");
            //the actual socket sending code next

            string xml = Encoding.UTF8.GetString(msgToSend);//decode the buffer into XML string
            Console.WriteLine("The coded and decoded string as XML:" + PrintXML(xml));

            Console.WriteLine("Encoded in HEX: " + BitConverter.ToString(msgToSend) + "\n");//http://stackoverflow.com/questions/623104/byte-to-hex-string

        }


        public static string PrintXML(string XML)//http://stackoverflow.com/questions/1123718/format-xml-string-to-print-friendly-xml-string
        {
            string Result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(XML);

                writer.Formatting = Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                string FormattedXML = sReader.ReadToEnd();

                Result = FormattedXML;
            }
            catch (XmlException)
            {
            }

            mStream.Close();
            writer.Close();

            return Result;
        }
    }
}
