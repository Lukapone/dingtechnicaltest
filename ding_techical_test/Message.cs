﻿using System;

namespace ding_techical_test
{
    public class Message
    {
        private string msgDate;
        private string msgTime;
        private string phoneNumber;
        private string amount;

        public string MsgDate
        {
            get
            {
                return msgDate;
            }

            set
            {
                msgDate = value;
            }
        }

        public string MsgTime
        {
            get
            {
                return msgTime;
            }

            set
            {
                msgTime = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }

            set
            {
                phoneNumber = value;
            }
        }

        public string Amount
        {
            get
            {
                return amount;
            }

            set
            {
                amount = value;
            }
        }

        public Message()
        {
            msgDate = "noDate";
            msgTime = "noTime";
            phoneNumber = "noPhoneNumber";
            amount = "noAmount";
        }

        public Message(string date, string time, string phoneNum, string p_amount)
        {
            msgDate = date;
            msgTime = time;
            phoneNumber = phoneNum;
            amount = p_amount;
        }

        public Message(string phoneNum, string p_amount)
        {
            dateTimeStampMessage();
            phoneNumber = phoneNum;
            amount = p_amount;
        }

        public void dateTimeStampMessage()
        {
            msgDate = DateTime.Now.ToString("ddMMyyyy"); 
            //msgTime = DateTime.Now.ToString("hh:mm:ss"); for better readibility if you printing it out
            msgTime = DateTime.Now.ToString("hhmmss");
        }

        public virtual void print()
        {
            Console.WriteLine("Message Date: {0} \nMessage Time: {1} \nPhone Number: {2} \nTopUp Amount: {3}", msgDate,msgTime,phoneNumber,amount);
        }

        //this method could check if the message is valid before sending it for example chack if PhoneNumber is numeric only and Amount have no cents
        public bool isValidMessage()
        {
            if (!IsDigitsOnly(phoneNumber))
            {
                Console.WriteLine("Invalid Phone Number");
                return false;
            }
            //here could be applied more check depending on the max amount and so on
            if (!IsDigitsOnly(amount))
            {
                Console.WriteLine("Invalid Amount");
                return false;
            }
            //the message is valid we can send it 
            return true;
        }

        bool IsDigitsOnly(string str)//http://stackoverflow.com/questions/7461080/fastest-way-to-check-if-string-contains-only-digits
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
}
