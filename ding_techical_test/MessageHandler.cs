﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ding_techical_test
{
    //class which handles building byte array from RequestMessage object and accepting byte array and creating ResponseMessage object
    class MessageHandler
    {
        public byte[] buildXMLRequest(RequestMessage msg)
        {
            return ConvertToBytes(msg.toXMLFormat());
        }

        private byte[] ConvertToBytes(XmlDocument doc)//http://stackoverflow.com/questions/1500259/how-to-convert-an-xmldocument-to-an-arraybyte
        {
            Encoding encoding = Encoding.UTF8;
            byte[] docAsBytes = encoding.GetBytes(doc.OuterXml);
            return docAsBytes;
        }

        //this method will take byte buffer from socket and parse it into ResponseMessage class
        public ResponseMessage parseByteArray(byte[] buffer)
        {
            XmlDocument doc = new XmlDocument();
            string xml = Encoding.UTF8.GetString(buffer);//decode the buffer into XML string
            //string xml = Encoding.Default.GetString(buffer); default encoding on your system
            string company = getCompanyFromResponse(xml);
            string xmlPart = getXMLPartFromResponse(xml);
            doc.LoadXml(xmlPart);

            return parseXMLElementsIntoMessage(doc);
        }

        
        //as the XML starts with company name as suppose to < bracket it is extracted here
        private string getCompanyFromResponse(string xmlResponse)
        {
            //if the xml does not start with < tag than I remove the beginning of it
            int index = xmlResponse.IndexOf('<');
            if (index > 0)
            {
                return xmlResponse.Substring(0, index);
            }

            return "";
        }

        //this will extract the the XML portion from the response
        private string getXMLPartFromResponse(string xmlResponse)
        {
            return xmlResponse.Substring(xmlResponse.IndexOf('<'));
        }

        //function to create ResponseMessage class from XML document
        private ResponseMessage parseXMLElementsIntoMessage(XmlDocument doc)
        {
            
            XmlNode root = doc.FirstChild;
            string date = "";
            string time = "";

            getResponseHeaderInfo(out date, out time, root.ChildNodes[0]);

            int transactionID = -1;
            int transactionNum = -1;
            string phoneNum = "";
            string amount = "";
            string result = "";

            getResponseBodyInfo(out transactionID, out transactionNum, out phoneNum, out amount, out result, root.ChildNodes[1]);

            return new ResponseMessage(date, time, transactionID, transactionNum, phoneNum, amount, result);

        }
   
        private void getResponseHeaderInfo(out string date, out string title, XmlNode header)
        {

            XmlElement headerElement = (XmlElement)header;
            date = headerElement["MessageDate"].InnerText;
            title = headerElement["MessageTime"].InnerText;

        }
        
        private void getResponseBodyInfo(out int transactionID, out int transactionNum, out string phoneNum, out string amount, out string result, XmlNode body)
        {
            XmlElement bodyElement = (XmlElement)body;
            string strTranID = bodyElement["TransactionID"].InnerText;
            string strTranNumber = bodyElement["TransactionNumber"].InnerText;
            int x = -1;
            if (int.TryParse(strTranID, out x))//http://stackoverflow.com/questions/1019793/how-can-i-convert-string-to-int
            {
                transactionID = x;
            }
            else
            {
                transactionID = x;
            }
            
            x = -1; // reset to not sucessfull
            if (int.TryParse(strTranNumber, out x))
            {
                transactionNum = x;
            }
            else
            {
                transactionNum = x;
            }

            phoneNum = bodyElement["PhoneNumber"].InnerText;
            amount = bodyElement["Amount"].InnerText;
            result = bodyElement["Result"].InnerText;
        }
    }
}
